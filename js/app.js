// Clef API personnelle
const APIKEY = '92d6baa0481fc63383c2a9c715b4a699';

// Variable de stockage de récuperation des datas demander
let apiCall = function(city){

// URL de récuperation des datas de l'API
let url = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${APIKEY}&units=metric&lang=fr`;

// Méthode .fetch pour récuperer les datas
fetch(url).then((response) =>
    response.json().then((data) => {
        // Récuperer l'icone du temps et la sauvegarder dans une variable.
        let weatherimg = data.weather[0].icon;
        // Calcul de la vitesse du vent en km/h
        let windspeed = Math.floor(data.wind.speed*3.6); 

        document.getElementById('boussole').style = "transform: rotate("+data.wind.deg+"deg)"


    console.log(data);

    // Récuperation de la data nom
    document.getElementById('city').innerHTML = data.name;

    // Récuperation de l'icone du temps 
    document.getElementById('ciel').innerHTML = `<img src="http://openweathermap.org/img/wn/${weatherimg}@2x.png">` + data.weather[0].description;

    // Récuperation de la data humidité
    document.getElementById('humidity').innerHTML = data.main.humidity + '%';

    // Récuperation de la data temperature ressentie
    document.getElementById('tempfeel').innerHTML = Math.floor(data.main.feels_like) + '°C';

    // Récuperation de la data température maximal
    document.getElementById('tempmax').innerHTML = Math.floor(data.main.temp_max) + '°C';

    // Récuperation de la data température minimal
    document.getElementById('tempmin').innerHTML = Math.floor(data.main.temp_min) + '°C';

    // Récuperation de la data direction du vent en degrès
    document.getElementById('windeg').innerHTML = data.wind.deg + '°';

    // Récuperation de la data vitesse du vent, modifié en km/h par la variable windspeed
    document.getElementById('windspeed').innerHTML = windspeed + 'km/h';
})
// catch du fetch, en cas d'erreur affiche l'erreur
).catch((err) => console.log('Erreur : '+ err));
};

// Moteur de recherche des villes dans l'API
document.querySelector('form').addEventListener('submit', function (e) {
    e.preventDefault();
    let ville = document.getElementById('inputCity').value;

    apiCall(ville);
});
// Appel les datas de la ville de Carbonne de base
apiCall('Carbonne');
